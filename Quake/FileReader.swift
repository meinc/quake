import Foundation

class FileReader {
    
    static func read(filePath: String) -> String? {
        guard !filePath.isEmpty else { return nil }
        guard let contentData = NSFileManager.defaultManager().contentsAtPath(filePath) else { return nil }
        return NSString(data: contentData, encoding: NSUTF8StringEncoding) as? String
    }
}