import Foundation
import ObjectMapper

class GamesLogReport {
    var games: [Game]
    var allRanksReport: String?
    var gamesReportJSONFormat: String?
    
    init(games: [Game]) {
        self.games = games
        allRanksReport = self.generateAllRanks()
        gamesReportJSONFormat = self.generateGamesToJSON()
    }
    
    private func generateAllRanks() -> String {
        var text = ""
        var count = 1
        
        for game in games {
            text += "Game: " + count.description + "\n"
            count += 1
            text += "*total kills: " + game.totalKills.description + "*"
            text += game.matchRankReport()
        }
        return text
    }
    
    private func generateGamesToJSON() -> String {
       return Mapper().toJSONString(games, prettyPrint: true)!
    }
}