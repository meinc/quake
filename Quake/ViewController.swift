import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var textView: UITextView!
    
    private var gamesLogReport: GamesLogReport?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let filePath = NSBundle.mainBundle().pathForResource("games", ofType: "log") {
            if let gamesLog = FileReader.read(filePath) {
                let games = GameLogParser.instance.setup(gamesLog)
                gamesLogReport = GamesLogReport(games: games)
                textView.text = gamesLogReport?.gamesReportJSONFormat
            } else {
                textView.text = "Error"
            }
        } else {
            textView.text = "Error"
        }
    }

    @IBAction func actionHandler(sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex
        {
            case 0:
                if let report = gamesLogReport?.gamesReportJSONFormat {
                    textView.text = report;
                } else {
                    textView.text = "Error";
                }
            case 1:
                if let ranks = gamesLogReport?.allRanksReport {
                    textView.text = ranks;
                } else {
                    textView.text = "Error";
                }
            
            default:
                break;
        }
    }
}

