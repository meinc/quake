import Foundation
import ObjectMapper

class Game: Mappable {
    static let WORLD = "<world>"
    var totalKills = 0
    var players = [String]()
    var kills = [String : Int]()
    var killsByMeans = [
        "MOD_UNKNOWN": 0,
        "MOD_SHOTGUN": 0,
        "MOD_GAUNTLET": 0,
        "MOD_MACHINEGUN": 0,
        "MOD_GRENADE": 0,
        "MOD_GRENADE_SPLASH": 0,
        "MOD_ROCKET": 0,
        "MOD_ROCKET_SPLASH": 0,
        "MOD_PLASMA": 0,
        "MOD_PLASMA_SPLASH": 0,
        "MOD_RAILGUN": 0,
        "MOD_LIGHTNING": 0,
        "MOD_BFG": 0,
        "MOD_BFG_SPLASH": 0,
        "MOD_WATER": 0,
        "MOD_SLIME": 0,
        "MOD_LAVA": 0,
        "MOD_CRUSH": 0,
        "MOD_TELEFRAG": 0,
        "MOD_FALLING": 0,
        "MOD_SUICIDE": 0,
        "MOD_TARGET_LASER": 0,
        "MOD_TRIGGER_HURT": 0,
        "MOD_NAIL": 0,
        "MOD_CHAINGUN": 0,
        "MOD_PROXIMITY_MINE": 0,
        "MOD_KAMIKAZE": 0,
        "MOD_JUICED": 0,
        "MOD_GRAPPLE": 0
    ]
    
    required init?(_ map: Map) {
        
    }
    
    init() {
        
    }
    
    func addPlayerIfNotExist(name: String) {
        for player in players {
            if player == name {
                return
            }
        }
        players.append(name)
        kills[name] = 0
    }
    
    func registerKillByMeans(mode: String) {
        if let value = killsByMeans[mode] {
            killsByMeans[mode] = value + 1
        }
    }
    
    func scorePositive(player: String) {
        var score = kills[player]!
        score += 1
        kills.updateValue(score, forKey: player)
    }
    
    func scoreNegative(player: String) {
        var score = kills[player]!
        score -= 1
        kills.updateValue(score, forKey: player)
    }
    
    func matchRankReport () -> String {
        let rank = kills.sort{ $0.1 > $1.1 }
        var text: String = ""
        
        text += "\n"
        for (player, kill) in rank {
            text += player + ": " + kill.description + "\n"
        }
        text += "\n"
        
        return text
    }
    
    func mapping(map: Map) {
        totalKills      <- map["total_kills"]
        players         <- map["players"]
        kills           <- map["kills"]
        killsByMeans    <- map["kills_by_means"]
    }
}