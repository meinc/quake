import Foundation

class GameLogParser {
    static let instance = GameLogParser()
    var games = [Game]()
    
    private init() {

    }
    
    func setup(gamesLog: String) -> [Game] {
        let gameLogArray = gamesLog.componentsSeparatedByCharactersInSet(NSCharacterSet.newlineCharacterSet())
        
        for item in gameLogArray {
            readNewGame(item)
            readKill(item)
        }
        
        return games
    }
    
    func readNewGame(item: String) -> Bool {
        guard !item.isEmpty else { return false }
        
        let regexInitGame = try! NSRegularExpression(pattern: " InitGame:", options: [])
        let matchesInitGame = regexInitGame.matchesInString(item, options: [], range: NSRange(location: 0, length: item.characters.count))
        
        guard !matchesInitGame.isEmpty else { return false }
        games.append(Game())
        return true
    }
    
    func readKill(item: String) -> Bool {
        guard !item.isEmpty else { return false }
        
        let regexKill = try! NSRegularExpression(pattern: " Kill:", options: [])
        let matchesKill = regexKill.matchesInString(item, options: [], range: NSRange(location: 0, length: item.characters.count))
        
        if !matchesKill.isEmpty {
            games.last?.totalKills += 1
            
            let indexBy = item.rangeOfString("by", options: .BackwardsSearch)?.startIndex
            var mode = item.substringWithRange(Range(indexBy! ..< item.characters.endIndex))
            mode = mode.stringByReplacingOccurrencesOfString("by ", withString: "")
            var newString = item.substringToIndex(indexBy!.advancedBy(-1))
            games.last?.registerKillByMeans(mode)
            
            let indexKilled = newString.rangeOfString("killed", options: .BackwardsSearch)?.startIndex
            var killed = newString.substringWithRange(Range(indexKilled! ..< newString.characters.endIndex))
            killed = killed.stringByReplacingOccurrencesOfString("killed ", withString: "")
            newString = newString.substringToIndex(indexKilled!.advancedBy(-1))
            games.last?.addPlayerIfNotExist(killed)
            
            let indexKiller = newString.rangeOfString(":", options: .BackwardsSearch)?.startIndex
            var killer = newString.substringWithRange(Range(indexKiller! ..< newString.characters.endIndex))
            killer = killer.stringByReplacingOccurrencesOfString(": ", withString: "")
            
            if killer != Game.WORLD {
                games.last?.addPlayerIfNotExist(killer)
                games.last?.scorePositive(killer)
            } else {
                games.last?.scoreNegative(killed)
            }
            return true
        }
        return false
    }
}