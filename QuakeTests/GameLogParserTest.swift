import Foundation

import XCTest
@testable import Quake

class GameLogParserTest: XCTestCase {
    var parser = GameLogParser.instance
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testReadNewGame() {
       
        var result = parser.readNewGame("")
        XCTAssertFalse(result)
        result = parser.readNewGame("0w2:07 IInitGame: adkadsdad")
        XCTAssertFalse(result)
        result = parser.readNewGame("21:42 Init: blabas;ksalabla")
        XCTAssertFalse(result)
        
        result = parser.readNewGame("20:37 InitGame: blablabla")
        XCTAssertTrue(result)
    }
    
    func testReadKill() {
        
        var result = parser.readKill("")
        XCTAssertFalse(result)
        result = parser.readKill("20:54 Kilaal: 1022 2 22: <world> killed Isgalamido by MOD_TRIGGER_HURT")
        XCTAssertFalse(result)
        result = parser.readKill("20:54 aaaKill: 1022 2 22: <world> killed Isgalamido by MOD_TRIGGER_HURT")
        XCTAssertFalse(result)
        
        result = parser.readKill("20:54 Kill: 1022 2 22: <world> killed Isgalamido by MOD_TRIGGER_HURT")
        XCTAssertTrue(result)
    }
}
