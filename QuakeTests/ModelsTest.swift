import XCTest
@testable import Quake

class ModelsTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testGameModel() {
        let game: Game? = Game()
        
        XCTAssertNotNil(game, "After initialization, game can't be nil")
        XCTAssertTrue((game?.players.isEmpty)!, "After creating a game, list of players names should be empty")
        XCTAssertTrue((game?.kills.isEmpty)!, "After creating a game, kills map shoulnd be empty")
    }
    
    func testGameModelMethodAddPlayer() {
        let game: Game? = Game()
        let playerNameFirst = "player 01"
        let playerNameSecond = "player 02"
        
        game?.addPlayerIfNotExist(playerNameFirst)
        XCTAssert(game?.players.count == 1, "After adding a the first player, count should be 1")
        
        game?.addPlayerIfNotExist(playerNameFirst)
        XCTAssert(game?.players.count == 1, "After adding a the first player for the second time, count should be 1")
        
        game?.addPlayerIfNotExist(playerNameSecond)
        XCTAssert(game?.players.count == 2, "After adding a the first player for the second time, count should be 2")
    }
    
    func testGameModelMethodScore() {
        let game: Game? = Game()
        let playerNameFirst = "player01"
        let playerNameSecond = "player02"
        
        game?.addPlayerIfNotExist(playerNameFirst)
        game?.addPlayerIfNotExist(playerNameSecond)
        
        game?.scorePositive(playerNameFirst)
        XCTAssert( game?.kills[playerNameFirst] == 1, "After adding a positive score to the first player, kills should be 1")
        XCTAssert( game?.kills[playerNameSecond] == 0, "After adding a positive score to the first player, second player kills should be 0")
        
        game?.scoreNegative(playerNameSecond)
        XCTAssert( game?.kills[playerNameFirst] == 1, "After adding a negative score to the second player, first player kills should be 1")
        XCTAssert( game?.kills[playerNameSecond] == -1, "After adding a negative score to the second player, kills should be -1")
    }
    
    func testGameModelMethodKillByMeans() {
        let game: Game? = Game()
        
        game?.registerKillByMeans("MOD_CRUSH")
        XCTAssert( game?.killsByMeans["MOD_CRUSH"] == 1, "After adding a positive score to the MOD, kills should be 1")
    }
}
