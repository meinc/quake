import XCTest
@testable import Quake

class FileReaderTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testRead() {
        let filePathExist = NSBundle.mainBundle().pathForResource("games", ofType: "log")
        let gamesLog = FileReader.read(filePathExist!)
        XCTAssertNotNil(gamesLog, "After read a valid log, games log can't be nil")
        XCTAssertFalse(gamesLog!.isEmpty, "After read a valid log, games log can't be empty")
        
        let filePathNotExist = filePathExist! + "fake"
        let gamesLogNotExist = FileReader.read(filePathNotExist)
        XCTAssertNil(gamesLogNotExist, "After read an invalid log, games log should be nil")
    }
}
